import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        width: width,
        alignment: Alignment.center, // where to position the child
        child: Center(
          child: Text("Spash text")
        ),
      ),
    );
  }
}