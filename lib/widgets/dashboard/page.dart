import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/dashboard/dashboard.dart';

import '../detail/page.dart';


class DashboardPage extends StatefulWidget {

  DashboardPage({Key key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}


class _DashboardPageState extends State<DashboardPage> {
  DashboardBloc _dashboardBloc;

  @override
  void initState() {
    _dashboardBloc = DashboardBloc();
    _dashboardBloc.dispatch(Fetch());
    super.initState();
  }

  Widget build(BuildContext context) {
    return BlocBuilder<DashboardBloc, DashboardState>(
      bloc: _dashboardBloc,
      builder: (BuildContext context, DashboardState state){
        if(state is DashboardUninitialized){
          return Center(child: CircularProgressIndicator(),);
        }else if(state is DashboardLoaded){
          return ListView(
            children: <Widget>[
              Container(
                height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.access_time,
                      color: Colors.blue,
                    ),
                    Text(
                      "  18/10/2019 12:12",
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              seperatorBox(),
              meterBox(),
              seperatorBox(),
              sensorsBox(),
              seperatorBox(),
              disabledWidget(icon: Icon(Icons.phonelink_ring, size: 50, color: Colors.blue), label: "Door"),
              seperatorBox(),
              disabledWidget(icon: Icon(Icons.ring_volume, size: 50, color: Colors.blue), label: "Motion"),
              seperatorBox(),
            ],
          );
        }

        return Container();
      }
    );



  }

  Widget seperatorBox(){
    return SizedBox(
      height: 2,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Colors.grey[200]
        ),
      ),
    );
  }

  Widget seperatorBox2(){
    return SizedBox(
      width: 2,
      height: 50,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Colors.grey[200]
        ),
      ),
    );
  }

  Widget meterBox(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(20),
          child: Text("Power Meter 1",style: TextStyle(color: Colors.orange,fontSize: 18),),
        ),
        meterRow(
          label: "V",
          title1: "Voltage",
          title2: "Volt",
          a1: "234",
          a2: "v_an",
          b1: "236",
          b2: "v_bn",
          c1: "236",
          c2: "v_cn",
        ),
        SizedBox(height: 25,),
        meterRow(
          label: "I",
          title1: "Current",
          title2: "Amp",
          a1: "49",
          a2: "i_a",
          b1: "46",
          b2: "i_b",
          c1: "32",
          c2: "i_c",
        ),
        SizedBox(height: 25,),
        meterRow(
          label: "O",
          title1: "Other",
          title2: "KWatt kWh",
          a1: "28",
          a2: "p_total",
          b1: "0",
          b2: "pf_total",
          c1: "557367",
          c2: "emergy\nactive",
        ),
        SizedBox(height: 20,),
        meterMore(),
        SizedBox(height: 20,),
      ],
    );
  }

  Widget meterRow({String label, String title1, String title2, String a1, String a2, String b1, String b2, String c1, String c2}){
    return InkWell(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return DetailPage();
        }));
      },
      child: Row(
        children: <Widget>[
          SizedBox(width: 15,),
          textIcon(label: label),
          SizedBox(width: 10,),
          Expanded(
            child: textDetailA(text1: title1,text2: title2)
          ),
          textDetailB(text1: a1,text2: a2),
          seperatorBox2(),
          textDetailB(text1: b1,text2: b2),
          seperatorBox2(),
          textDetailB(text1: c1,text2: c2),
        ],
      )
    );
  }

  Widget meterMore(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 75),
          child: Text("More parameter",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
        ),
        Container(
          padding: EdgeInsets.only(right: 20),
          child: IconButton(
            icon: Icon(Icons.arrow_forward_ios),
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return DetailPage();
              }));
            },
          ),
        )

      ],
    );
  }

  Widget textIcon({String label}){
    return Container(
      width: 50,
      height: 50,
      child: Center(
        child: Text(label,style: TextStyle(color: Colors.white,fontSize: 25,fontWeight: FontWeight.bold),),
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.blue
      ),
    );
  }

  Widget textDetailA({String text1,String text2}){
    return Container(
      width: 80,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(text1,style:TextStyle(fontSize: 19,fontWeight: FontWeight.bold),),
          SizedBox(height: 8,),
          Text(text2,style:TextStyle(color: Colors.black54))
        ],
      )
    );
  }

  Widget textDetailB({String text1,String text2}){
    return Container(
      width: 70,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(text1,style:TextStyle(fontSize: 19,fontWeight: FontWeight.bold),),
          SizedBox(height: 8,),
          Text(text2,style:TextStyle(color: Colors.black54))
        ],
      )
    );
  }

  Widget sensorsBox(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(20),
          child: Text("Sensors 1",style: TextStyle(color: Colors.green,fontSize: 18),),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            sensorBoxColumn(icon: Icon(Icons.cloud,size: 50,), mainText: "31°C", subText: "High Temp"),
            sensorBoxColumn(icon: Icon(Icons.highlight,size: 50), mainText: "30°C", subText: "Ambient"),
            sensorBoxColumn(icon: Icon(Icons.beach_access,size: 50), mainText: "85%", subText: "Humidity"),
          ],
        ),
        SizedBox(height: 20,)
      ]
    );
  }

  Widget sensorBoxColumn({Icon icon, String mainText, String subText}){
    return Column(
      children: <Widget>[
        icon,
        Text(mainText,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
        SizedBox(height: 5,),
        Text(subText,style: TextStyle(color: Colors.black54)),
      ],
    );
  }

  Widget disabledWidget({Icon icon, String label}){
    return Column(
      children: <Widget>[
        SizedBox(height: 15,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(width: 15,),
            icon,
            SizedBox(width: 10,),

            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(label,style:TextStyle(fontSize: 19,fontWeight: FontWeight.bold, color: Colors.blue[500])),
                  Text("OFF",style:TextStyle(fontSize: 19)),
                ],
              ),
            ),
            Icon(Icons.arrow_forward_ios, size: 30, color: Colors.blue),
            SizedBox(width: 15,),
          ],
        ),
        SizedBox(height: 15,),
      ],
    );
    /*
    return Column(
      children: <Widget>[
        Icon(Icons.phonelink_ring, size: 50, color: Colors.blue)
      ],
    );
    */
  }

  void dispose() {
    _dashboardBloc.dispose();
    super.dispose();
  }
}