import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mockup_mdb/widgets/settings/page.dart';

import '../../bloc/home/home.dart';
import '../../bloc/authentication/authentication.dart';

import '../dashboard/page.dart';
import '../settings/page.dart';
import '../Alarm/page.dart';

class HomePage extends StatefulWidget {

  HomePage({Key key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  AuthenticationBloc _authenticationBloc;
  MainBloc _mainBloc;

  @override
  void initState() {
    _mainBloc = MainBloc();
    _mainBloc.stateIndex = 1;
    _mainBloc.dispatch(ChangeToMain());
    _authenticationBloc =BlocProvider.of<AuthenticationBloc>(context);
    super.initState();
  }

  Future<bool> _onWillPop() {
    if(_mainBloc.stateIndex != 1){
      _mainBloc.dispatch(ChangeStateWithIndex(index: 1));
      return Future.value(false);
    }

    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: BlocProvider<MainBloc>(
        builder: (BuildContext context) => _mainBloc,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: BlocBuilder<MainBloc, MainState>(
              bloc: _mainBloc,
              builder: (BuildContext context, MainState state){
                if(state is MainDashboard){
                  return Text("Dashboard");
                }else if(state is Settings){
                  return Text("Settings");
                }else if(state is Alarm){
                  return Text("Alarm summary");
                }
                return Container();
              }
            ),
            leading: IconButton(
              icon: Icon(Icons.person),
              onPressed: () {
                _scaffoldKey.currentState.openDrawer();
              },
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.dashboard),
                onPressed: (){
                  _mainBloc.dispatch(ChangeStateWithIndex(index: 1));
                },
              )
            ],
          ),
          drawer: getDrawer(),

          body: BlocBuilder<MainBloc, MainState>(
            bloc: _mainBloc,
            builder: (BuildContext context, MainState state){
              if(state is MainDashboard){
                return DashboardPage();
              }else if(state is Settings){
                return SettingsPage();
              }else if(state is Alarm){
                return AlarmPage();
              }

              return Container();
            }
          ),

          bottomNavigationBar: bottomNavigation()
        ),

      )
    );
  }

  Widget getDrawer(){
    return Drawer(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            child: const Text('Sign out'),
            onPressed: (){
              _authenticationBloc.dispatch(LoggedOut());
            },
          )
      ],)
    );
  }

  Widget bottomNavigation(){
    return BlocBuilder<MainBloc, MainState>(
      bloc: _mainBloc,
      builder: (BuildContext context, MainState state){
        return  BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.alarm),
              title: Text('Alert'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              title: Text('Settings'),
            ),
          ],
          currentIndex:_mainBloc.stateIndex,
          onTap: onTabTapped,
        );
      }
    );
  }

  void onTabTapped(int index) {
    _mainBloc.dispatch(ChangeStateWithIndex(index: index));
  }

  @override
  void dispose() {
    _mainBloc.dispose();
    super.dispose();
  }
}