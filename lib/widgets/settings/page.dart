import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/settings/settings.dart';


class SettingsPage extends StatefulWidget {

  SettingsPage({Key key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}


class _SettingsPageState extends State<SettingsPage> {
  SettingsBloc _settingsBloc;

  @override
  void initState() {
    _settingsBloc = SettingsBloc();
    _settingsBloc.dispatch(Fetch());
    super.initState();
  }

  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      bloc: _settingsBloc,
      builder: (BuildContext context, SettingsState state){
        if(state is SettingsUninitialized){
          return Center(child: CircularProgressIndicator(),);
        }else if(state is SettingsLoaded){
          return Center(child: Text('Settings'),);
        }
        return Container();
      }
    );
  }

  void dispose() {
    _settingsBloc.dispose();
    super.dispose();
  }
}