import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../repository/user.dart';
import '../../bloc/authentication/authentication.dart';
import '../../bloc/login/login.dart';

class LoginPage extends StatefulWidget {
  final UserRepository userRepository;

  LoginPage({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginBloc _loginBloc;
  AuthenticationBloc _authenticationBloc;

  UserRepository get _userRepository => widget.userRepository;

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _loginBloc = LoginBloc(
      userRepository: _userRepository,
      authenticationBloc: _authenticationBloc,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(color: Colors.greenAccent),
                height: height/3,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white
                    ),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: (width/10 < 100 )?100:width/10,
                        ),
                        Container(
                          width: 300,
                          child: TextField(
                            decoration: InputDecoration(
                              hintText: 'Email address',
                              suffixIcon: Icon(
                                Icons.email
                              )
                            ),
                          ),
                        ),

                        SizedBox(height: 20,),
                        Container(
                          width: 300,
                          child: TextField(
                            obscureText: true,
                            decoration: InputDecoration(
                              hintText: 'Password',
                              suffixIcon: IconButton(
                                icon: Icon(Icons.remove_red_eye),
                                onPressed: () {

                                }
                              )
                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        RaisedButton(
                          child: Text("Login",style: TextStyle(color: Colors.white),),
                          onPressed: (){
                             _loginBloc.dispatch(LoginButtonPressed(
                               username: "",
                               password: "",
                             ));
                          },
                          color: Colors.blue,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                        ),
                        SizedBox(height: 30,),
                        Text('forget password?',style: TextStyle(color: Colors.blueAccent),)
                      ],
                    ),
                  ),
                )
              )
            ],
          ),
          Positioned(
            left: width/2 - 75,
            top: height/3 - 75,
            child: Container(
              width: 150,
              height: 150,
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle
              ),
              child: Icon(
                Icons.developer_board,
                color: Colors.greenAccent,
                size: 100,
              ),
            )
          )

        ],
      )
    );
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }
}