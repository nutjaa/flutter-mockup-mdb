import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'graph.dart';

import '../../bloc/detail/detail.dart';

class DetailPage extends StatefulWidget {
  DetailPage({Key key}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  DetailBloc _detailBloc;

  @override
  void initState() {
    _detailBloc = DetailBloc();
    _detailBloc.dispatch(Fetch());
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Power Meter 1 : Current A"),
      ),
      body: BlocBuilder<DetailBloc, DetailState>(
        bloc: _detailBloc,
        builder: (BuildContext context, DetailState state){
          if(state is DetailUninitialized){
            return Center(child: CircularProgressIndicator(),);
          }else if(state is DetailLoaded){
            return ListView(
              children: <Widget>[
                SizedBox(height: 15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 50,
                      height: 50,
                      child: Center(
                        child: Text("I",style: TextStyle(color: Colors.white,fontSize: 25,fontWeight: FontWeight.bold),),
                      ),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blue
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text("49.418",style: TextStyle(color: Colors.blue,fontSize: 30,fontWeight: FontWeight.bold))
                  ],
                ),
                SizedBox(height: 15,),
                Center(child: Text("Current A (A)"),),
                SizedBox(height: 15,),
                Center(child: Text("update 16/10/2019 10:01"),),
                SizedBox(height: 20,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 15,),
                    Expanded(
                      child: Text("Transaction histori",style: TextStyle(color: Colors.blue, fontSize: 18),),
                    ),
                    Text("Detail",style: TextStyle(color: Colors.black54),),
                    Icon(Icons.keyboard_arrow_right,color: Colors.black54,)
                  ],
                ),
                Container(
                  height: 350,
                  padding: EdgeInsets.all(20),
                  child: SimpleTimeSeriesChart.withSampleData(),
                ),
                Container(
                  padding: EdgeInsets.only(left: 15,bottom: 20),
                  child: Text("Acknowledged alarm",style: TextStyle(color: Colors.blue, fontSize: 18)),
                ),
                seperatorBox(),
                SizedBox(height: 10,),
                alramBox(),
                SizedBox(height: 10,),
                alramBox(),
                SizedBox(height: 10,),
                alramBox(),
                SizedBox(height: 10,),
                alramBox(),
                SizedBox(height: 10,),
                alramBox(),
                SizedBox(height: 10,),
                alramBox(),
                SizedBox(height: 10,),
                alramBox(),
                SizedBox(height: 10,),
                alramBox(),
                SizedBox(height: 10,),
              ]
            );
          }
          return Container();
        }
      )
    );
  }

  Widget seperatorBox(){
    return SizedBox(
      height: 2,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Colors.grey[200]
        ),
      ),
    );
  }

  Widget alramBox(){
    return Row(
      children: <Widget>[
        SizedBox(width: 15,),
        Container(
          width: 40,
          height: 40,
          child: Center(
            child: Icon(Icons.priority_high,color: Colors.white,),
          ),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.blue
          ),
        ),
        SizedBox(width: 20,),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Current A",style: TextStyle(fontSize: 17),),
              SizedBox(height: 3,),
              Text("Alarm Low 44.372 A",style: TextStyle(color: Colors.black54)),
              SizedBox(height: 3,),
              Text("30/04/2019 10:29",style: TextStyle(color: Colors.black54))
            ],
          ),
        )
      ],
    );
  }
}