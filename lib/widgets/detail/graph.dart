/// Timeseries chart example
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'dart:math';

class SimpleTimeSeriesChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  SimpleTimeSeriesChart(this.seriesList, {this.animate});

  /// Creates a [TimeSeriesChart] with sample data and no transition.
  factory SimpleTimeSeriesChart.withSampleData() {
    return SimpleTimeSeriesChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }


  @override
  Widget build(BuildContext context) {
    return charts.TimeSeriesChart(
      seriesList,
      animate: animate,
      // Optionally pass in a [DateTimeFactory] used by the chart. The factory
      // should create the same type of [DateTime] as the data provided. If none
      // specified, the default creates local date time.
      dateTimeFactory: const charts.LocalDateTimeFactory(),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<TimeSeriesSales, DateTime>> _createSampleData() {
    var rng = Random();
    final data = [
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 30), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 31), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 32), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 33), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 34), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 35), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 36), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 37), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 38), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 39), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 40), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 41), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 42), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 43), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 44), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 45), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 46), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 47), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 48), rng.nextInt(100)),
      TimeSeriesSales(DateTime(2019, 10, 15, 8, 49), rng.nextInt(100)),
    ];

    return [
      charts.Series<TimeSeriesSales, DateTime>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
}

/// Sample time series data type.
class TimeSeriesSales {
  final DateTime time;
  final int sales;

  TimeSeriesSales(this.time, this.sales);
}