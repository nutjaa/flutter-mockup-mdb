import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/alarm/alarm.dart';


class AlarmPage extends StatefulWidget {

  AlarmPage({Key key}) : super(key: key);

  @override
  State<AlarmPage> createState() => _AlarmPagePageState();
}


class _AlarmPagePageState extends State<AlarmPage> {
  AlarmBloc _alarmBloc;

  @override
  void initState() {
    _alarmBloc = AlarmBloc();
    _alarmBloc.dispatch(Fetch());
    super.initState();
  }

  Widget build(BuildContext context) {
    return BlocBuilder<AlarmBloc, AlarmState>(
      bloc: _alarmBloc,
      builder: (BuildContext context, AlarmState state){
        if(state is AlarmUninitialized){
          return Center(child: CircularProgressIndicator(),);
        }else if(state is AlarmLoaded){
          return ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(15),
                child: Text('Acknowledge alarm',style: TextStyle(color: Colors.blue[600],fontWeight: FontWeight.bold),),
              ),
              seperatorBox(),
              alarmListItem(icon: warningBlue(),name1: "Voltage A-B", name2: "Alarm Low 0 V", name3: "Power Meter 1", date1: "20/10/2019", date2: "23:21"),
              alarmListItem(icon: warningBlue(),name1: "Current A", name2: "Alarm Low 44.372 A", name3: "Power Meter 1", date1: "20/10/2019", date2: "22:21"),
              alarmListItem(icon: warningRed(),name1: "Voltage B-C", name2: "Alarm High 404.844 V", name3: "Power Meter 1", date1: "20/10/2019", date2: "22:15"),
              alarmListItem(icon: warningBlue(),name1: "Temperature (Ambiant)", name2: "Alarm Low 0 C", name3: "Power Meter 1", date1: "20/10/2019", date2: "21:21"),
              alarmListItem(icon: warningRedBorder(),name1: "Current B", name2: "Warning High 44.533 A", name3: "Power Meter 1", date1: "20/10/2019", date2: "20:15"),
              Container(
                padding: EdgeInsets.all(15),
                child: Text('History alarm',style: TextStyle(color: Colors.yellow[700],fontWeight: FontWeight.bold),),
              ),
              seperatorBox(),
              alarmListItem(icon: warningGreyBorder(),name1: "Voltage A-B", name2: "Alarm Low 44.372 A", name3: "Power Meter 1", date1: "19/10/2019", date2: "20:15"),
              alarmListItem(icon: warningGreyBorder(),name1: "Voltage A-B", name2: "Alarm Low 44.372 A", name3: "Power Meter 1", date1: "19/10/2019", date2: "19:15"),
              alarmListItem(icon: warningGreyBorder(),name1: "Voltage A-B", name2: "Alarm Low 44.372 A", name3: "Power Meter 1", date1: "19/10/2019", date2: "18:15"),
              alarmListItem(icon: warningGreyBorder(),name1: "Voltage A-B", name2: "Alarm Low 44.372 A", name3: "Power Meter 1", date1: "19/10/2019", date2: "17:15"),
              alarmListItem(icon: warningGreyBorder(),name1: "Voltage A-B", name2: "Alarm Low 44.372 A", name3: "Power Meter 1", date1: "19/10/2019", date2: "16:15"),
              alarmListItem(icon: warningGreyBorder(),name1: "Voltage A-B", name2: "Alarm Low 44.372 A", name3: "Power Meter 1", date1: "19/10/2019", date2: "15:15"),
              alarmListItem(icon: warningGreyBorder(),name1: "Voltage A-B", name2: "Alarm Low 44.372 A", name3: "Power Meter 1", date1: "19/10/2019", date2: "14:15"),
            ]
          );
        }
        return Container();
      }
    );
  }

  Widget seperatorBox(){
    return SizedBox(
      height: 2,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Colors.grey[200]
        ),
      ),
    );
  }

  TextStyle lightNormalText(){
    return TextStyle(color: Colors.black54);
  }

  Widget alarmListItem({Widget icon,String name1,String name2, String name3, String date1, String date2}){
    return Container(
      padding: EdgeInsets.fromLTRB(15, 10, 10, 10),
      child: Row(
      children: <Widget>[
          icon,
          SizedBox(width: 10,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(name1,style: TextStyle(fontWeight: FontWeight.bold),),
                SizedBox(height: 5,),
                Text(name2,style: lightNormalText(),),
                SizedBox(height: 5,),
                Text(name3,style: lightNormalText())
              ],
            )
          ,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(date1, style: lightNormalText()),
              SizedBox(height: 5,),
              Text(date2,style: lightNormalText())
            ],
          )
      ],)
    );
  }

  Widget warningBlue(){
    return Container(
      width: 50,
      height: 50,
      child: Center(
        child: Text("!",style: TextStyle(color: Colors.white,fontSize: 25,fontWeight: FontWeight.bold),),
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.blue[700]
      ),
    );
  }

  Widget warningRed(){
    return Container(
      width: 50,
      height: 50,
      child: Center(
        child: Text("!",style: TextStyle(color: Colors.white,fontSize: 25,fontWeight: FontWeight.bold),),
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.red
      ),
    );
  }

  Widget warningRedBorder(){
    return Container(
      width: 50,
      height: 50,
      padding: EdgeInsets.all(4),
      child: Container(
        width: 42,
        height: 42,
        child: Center(
          child: Text("!",style: TextStyle(color: Colors.red,fontSize: 25,fontWeight: FontWeight.bold),),
        ),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white
        ),
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.red
      ),
    );
  }

  Widget warningGreyBorder(){
    return Container(
      width: 50,
      height: 50,
      padding: EdgeInsets.all(4),
      child: Container(
        width: 42,
        height: 42,
        child: Center(
          child: Text("!",style: TextStyle(color: Colors.grey,fontSize: 25,fontWeight: FontWeight.bold),),
        ),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white
        ),
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey
      ),
    );
  }

  void dispose() {
    _alarmBloc.dispose();
    super.dispose();
  }
}