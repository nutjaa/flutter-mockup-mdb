import 'dart:async';
import 'package:bloc/bloc.dart';

import './dashboard.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  int stateIndex;

  @override
  DashboardState get initialState => DashboardUninitialized();

  @override
  Stream<DashboardState> mapEventToState(
    DashboardEvent event,
  ) async* {

    if(event is Fetch){
      yield DashboardUninitialized();
      await Future.delayed(const Duration(seconds: 2));
      yield DashboardLoaded();
    }
  }
}