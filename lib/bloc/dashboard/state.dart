abstract class DashboardState{
  DashboardState([List props = const []]);
}

class DashboardUninitialized extends DashboardState {
  @override
  String toString() => 'DashboardUninitialized';
}

class DashboardError extends DashboardState {
  @override
  String toString() => 'DashboardError';
}

class DashboardLoaded extends DashboardState {
  @override
  String toString() => 'DashboardLoaded';
}
