import 'package:equatable/equatable.dart';

abstract class DashboardEvent extends Equatable {}

class Fetch extends DashboardEvent {

  Fetch();

  @override
  String toString() => 'Fetch';
}