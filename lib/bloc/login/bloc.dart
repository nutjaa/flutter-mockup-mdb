import 'dart:async';

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import '../../repository/user.dart';
import '../../bloc/authentication/authentication.dart';
import '../../bloc/login/login.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  })  : assert(userRepository != null),
        assert(authenticationBloc != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();
      authenticationBloc.dispatch(LoggedIn(token: ""));
      yield LoginInitial();
    }
  }
}