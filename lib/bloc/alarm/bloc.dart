import 'dart:async';
import 'package:bloc/bloc.dart';

import './alarm.dart';

class AlarmBloc extends Bloc<AlarmEvent, AlarmState> {

  @override
  AlarmState get initialState => AlarmUninitialized();

  @override
  Stream<AlarmState> mapEventToState(
    AlarmEvent event,
  ) async* {

    if(event is Fetch){
      yield AlarmUninitialized();
      await Future.delayed(const Duration(seconds: 1));
      yield AlarmLoaded();
    }
  }
}