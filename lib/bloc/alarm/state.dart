abstract class AlarmState{
  AlarmState([List props = const []]);
}

class AlarmUninitialized extends AlarmState {
  @override
  String toString() => 'AlarmUninitialized';
}

class AlarmError extends AlarmState {
  @override
  String toString() => 'AlarmError';
}

class AlarmLoaded extends AlarmState {
  @override
  String toString() => 'AlarmLoaded';
}
