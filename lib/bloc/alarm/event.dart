import 'package:equatable/equatable.dart';

abstract class AlarmEvent extends Equatable {}

class Fetch extends AlarmEvent {

  Fetch();

  @override
  String toString() => 'Fetch';
}