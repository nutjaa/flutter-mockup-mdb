import 'dart:async';
import 'package:bloc/bloc.dart';

import './settings.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {

  @override
  SettingsState get initialState => SettingsUninitialized();

  @override
  Stream<SettingsState> mapEventToState(
    SettingsEvent event,
  ) async* {

    if(event is Fetch){
      yield SettingsUninitialized();
      await Future.delayed(const Duration(seconds: 1));
      yield SettingsLoaded();
    }
  }
}