abstract class SettingsState{
  SettingsState([List props = const []]);
}

class SettingsUninitialized extends SettingsState {
  @override
  String toString() => 'SettingsUninitialized';
}

class SettingsError extends SettingsState {
  @override
  String toString() => 'SettingsError';
}

class SettingsLoaded extends SettingsState {
  @override
  String toString() => 'SettingsLoaded';
}
