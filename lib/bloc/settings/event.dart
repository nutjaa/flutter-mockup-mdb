import 'package:equatable/equatable.dart';

abstract class SettingsEvent extends Equatable {}

class Fetch extends SettingsEvent {

  Fetch();

  @override
  String toString() => 'Fetch';
}