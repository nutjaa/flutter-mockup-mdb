import 'dart:async';
import 'package:bloc/bloc.dart';

import './detail.dart';

class DetailBloc extends Bloc<DetailEvent, DetailState> {
  int stateIndex;

  @override
  DetailState get initialState => DetailUninitialized();

  @override
  Stream<DetailState> mapEventToState(
    DetailEvent event,
  ) async* {

    if(event is Fetch){
      yield DetailUninitialized();
      await Future.delayed(const Duration(seconds: 2));
      yield DetailLoaded();
    }
  }
}