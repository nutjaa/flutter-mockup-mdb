import 'package:equatable/equatable.dart';

abstract class DetailEvent extends Equatable {}

class Fetch extends DetailEvent {

  Fetch();

  @override
  String toString() => 'Fetch';
}