abstract class DetailState{
  DetailState([List props = const []]);
}

class DetailUninitialized extends DetailState {
  @override
  String toString() => 'DetailUninitialized';
}

class DetailError extends DetailState {
  @override
  String toString() => 'DetailError';
}

class DetailLoaded extends DetailState {
  @override
  String toString() => 'DetailLoaded';
}
