import 'dart:async';
import 'package:bloc/bloc.dart';

import 'home.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  int stateIndex;

  @override
  MainState get initialState => MainDashboard();

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {

    if(event is ChangeToMain){
      stateIndex = 1;
      yield MainDashboard();
    }

    if(event is ChangeStateWithIndex){
      print(event.index);
      switch (event.index) {
        case 0:
          yield Alarm();
          break;
        case 1:
          yield MainDashboard();
          break;
        case 2:
          yield Settings();
          break;
      }
      stateIndex = event.index;
    }
  }
}