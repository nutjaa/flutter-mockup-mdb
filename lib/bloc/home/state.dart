import 'package:equatable/equatable.dart';

abstract class MainState extends Equatable {}


class MainDashboard extends MainState {
  @override
  String toString() => 'MainDashboard state';
}

class Alarm extends MainState {
  @override
  String toString() => 'Alarm';
}

class Settings extends MainState {
  @override
  String toString() => 'Settings';
}