import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class MainEvent extends Equatable {
  MainEvent([List props = const []]) : super(props);
}

class ChangeToMain extends MainEvent {
  @override
  String toString() => 'Select main page';
}

class ChangeStateWithIndex extends MainEvent {
  final int index;

  ChangeStateWithIndex({
    @required this.index,
  }) : super([index]);

  @override
  String toString() =>
      'ChangeStateWithIndex { index: $index }';
}



